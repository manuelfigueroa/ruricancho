<?php

namespace Modules\Sale\Http\Controllers;

use Modules\Purchase\Models\PurchaseOrder;

use App\Http\Controllers\SearchItemController;
use App\Http\Controllers\Tenant\EmailController;
use App\Models\Tenant\Configuration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tenant\Person;
use App\Models\Tenant\Catalogs\CurrencyType;
use App\Models\Tenant\Catalogs\ChargeDiscountType;
use App\Models\Tenant\Establishment;
use App\CoreFacturalo\Requests\Inputs\Common\LegendInput;
use App\Models\Tenant\Item;
use App\Models\Tenant\Series;
use App\Models\Tenant\Catalogs\AffectationIgvType;
use App\Models\Tenant\Catalogs\DocumentType;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant\Catalogs\PriceType;
use App\Models\Tenant\Catalogs\SystemIscType;
use App\Models\Tenant\Catalogs\AttributeType;
use App\Models\Tenant\Company;
use App\Models\Tenant\Warehouse;
use Illuminate\Support\Str;
use App\CoreFacturalo\Requests\Inputs\Common\PersonInput;
use App\CoreFacturalo\Requests\Inputs\Common\EstablishmentInput;
use App\CoreFacturalo\Helpers\Storage\StorageDocument;
use App\CoreFacturalo\Template;
use Mpdf\Mpdf;
use Mpdf\HTMLParserMode;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\Models\Tenant\PaymentMethodType;
use Modules\Sale\Models\SaleOpportunity;
use Modules\Sale\Models\OP;
use Modules\Sale\Models\Color;
use Modules\Sale\Models\Hilo;
use Modules\Sale\Models\Tela;
use Modules\Sale\Models\ProcesoProd;
use Modules\Sale\Models\ProcesoProduc;
use Modules\Sale\Models\SaleOpportunityItem;
use Modules\Sale\Http\Resources\SaleOpportunityCollection;
use Modules\Sale\Http\Resources\ProcesoProdCollection;
use Modules\Sale\Http\Resources\ProcesoProducCollection;
use Modules\Sale\Http\Resources\SaleOpportunityResource;
use Modules\Sale\Http\Resources\SaleOpportunityResource2;
use Modules\Sale\Http\Requests\SaleOpportunityRequest;
use Modules\Sale\Http\Requests\ProcesoProducCreateRequest;
use Modules\Sale\Http\Requests\ProcesoProducImportRequest;
use Modules\Sale\Http\Requests\ProcesoProducTejRequest;
use Modules\Sale\Http\Requests\ProcesoProducTinRequest;
use Modules\Sale\Http\Requests\ProcesoProducAlmRequest;
use App\Http\Requests\Tenant\ItemRequest;
use App\Models\Tenant\Purchase;
use App\Models\Tenant\PurchaseItem;

use Modules\Sale\Mail\SaleOpportunityEmail;

/**
 * Class SaleOpportunityController
 *
 * @package Modules\Sale\Http\Controllers
 * @mixin Controller
 */
class ProdController extends Controller
{
    public function index(){

        return view('tenant.proceso_prod.index');
    }
    public function create($id=null)
    {
        return view('tenant.proceso_prod.form', compact('id'));
    }
    public function import($id)
    {
        //dd($id);
        return view('tenant.proceso_prod.import', compact('id'));
    }
    public function tej($id)
    {
        return view('tenant.proceso_prod.tej', compact('id'));
    }
    public function tin($id )
    {
        return view('tenant.proceso_prod.tin', compact('id'));
    }
    public function alm($id)
    {
        return view('tenant.proceso_prod.alm', compact('id'));
    }
    public function det($id)
    {
        return view('tenant.proceso_prod.det', compact('id'));
    }
    public function record($id)
    {
        $record =  ProcesoProduc::find($id);

        return $record;
    }
    public function columns()
    {
        return [
            'fecha_inicio' => 'Fecha de inicio',
            'op' => 'Proveedor'
        ];
    }
    public function records(Request $request)
    {
        // $records =DB::table('proceso_producs')->get();
        $records = ProcesoProduc::where('id', '>',0);
         return new ProcesoProducCollection($records->paginate(config('tenant.items_per_page')));
        // $records = $this->getRecords($request);

        // return new ProcesoProdCollection($records->paginate(config('tenant.items_per_page')));
    }
    

    private function getRecords($request){

        if($request->column == 'user_name'){

            $records = ProcesoProd::whereHas('user', function($query) use($request){
                            $query->where('name', 'like', "%{$request->value}%");
                        })
                        ->whereTypeUser()
                        ->latest();

        }elseif($request->column == 'customer_name'){

            $records =ProcesoProd::whereHas('person', function($query) use($request){
                            $query->where('name', 'like', "%{$request->value}%");
                        })
                        ->whereTypeUser()
                        ->latest();

        }else{

            $records = ProcesoProd::where($request->column, 'like', "%{$request->value}%")
                                ->whereTypeUser()
                                ->latest();

        }
        $records = ProcesoProduc::get();

        return $records;
    } 
     public function item_tables() {

        $items = $this->table('items');
       // $items = SearchItemController::getNotServiceItemToModal();

       $affectation_igv_types = AffectationIgvType::whereActive()->get();
       $price_types = PriceType::whereActive()->get();

       return compact('items', 'affectation_igv_types', 'price_types');
   }

   public function tables() {
    $purchases = Purchase::query()->select('id' )->get()->transform(function($row) {
        return [
            'id' => $row->id,
        ];
    });
    // $purchase_items = PurchaseItem::query()->select('purchase_id','item' )->get()->transform(function($row) {
    //     return [
    //         'purchase_id' => $row->purchase_id,
    //         'item' => $row->item,
    //     ];
    // });
    $purchase_items = PurchaseItem::all();
    $tela = Tela::all();
    $hilo = Hilo::all();
    $color = Color::all();
    $op = OP::all();
    $warehouses = Warehouse::query()->select('establishment_id', 'description')->get()->transform(function($row) {
        return [
            'id' => $row->establishment_id,
            'description' => $row->description
        ];
    });
    $customers = $this->table('customers');
    $suppliers = $this->table('suppliers');
    $purchase_orders = PurchaseOrder::query()->select('id', 'prefix')->get()->transform(function($row) {
        return [
            'id' => $row->id,
            'number' => $row->prefix.' - '.$row->id
        ];
    });
    $establishments = Establishment::where('id', auth()->user()->establishment_id)->get();
    $currency_types = CurrencyType::query()->select('id', 'description', 'symbol')->whereActive()->get()->transform(function($row) {
        return [
            'id' => $row->id,
            'name' => $row->description.' '.$row->symbol
        ];
    });
    $company = Company::active();

    return compact('purchases','purchase_items','op','tela', 'hilo', 'color', 'warehouses', 'purchase_orders', 'suppliers', 'customers', 'establishments','currency_types','company');
    }

    public function table($table)
    {
        switch ($table) {
            case 'suppliers':

                $customers = Person::whereType('suppliers')->orderBy('name')->take(20)->get()->transform(function($row) {
                    return [
                        'id' => $row->id,
                        'description' => $row->number.' - '.$row->name,
                        'name' => $row->name,
                        'number' => $row->number,
                        'identity_document_type_id' => $row->identity_document_type_id,
                        'identity_document_type_code' => $row->identity_document_type->code
                    ];
                });
                return $customers;

                break;
            case 'customers':

                $customers = Person::whereType('customers')->orderBy('name')->take(20)->get()->transform(function($row) {
                    return [
                        'id' => $row->id,
                        'description' => $row->number.' - '.$row->name,
                        'name' => $row->name,
                        'number' => $row->number,
                        'identity_document_type_id' => $row->identity_document_type_id,
                        'identity_document_type_code' => $row->identity_document_type->code
                    ];
                });
                return $customers;

                break;

            case 'items':

                $warehouse = Warehouse::where('establishment_id', auth()->user()->establishment_id)->first();

                $items = Item::orderBy('description')->whereIsActive()->whereNotIsSet()
                    // ->with(['warehouses' => function($query) use($warehouse){
                    //     return $query->where('warehouse_id', $warehouse->id);
                    // }])
                    ->get()->transform(function($row) {
                    $full_description = $this->getFullDescription($row);
                    // $full_description = ($row->internal_id)?$row->internal_id.' - '.$row->description:$row->description;
                    return [
                        'id' => $row->id,
                        'full_description' => $full_description,
                        'description' => $row->description,
                        'currency_type_id' => $row->currency_type_id,
                        'currency_type_symbol' => $row->currency_type->symbol,
                        'sale_unit_price' => $row->sale_unit_price,
                        'purchase_unit_price' => $row->purchase_unit_price,
                        'unit_type_id' => $row->unit_type_id,
                        'sale_affectation_igv_type_id' => $row->sale_affectation_igv_type_id,
                        'purchase_affectation_igv_type_id' => $row->purchase_affectation_igv_type_id,
                        'is_set' => (bool) $row->is_set,
                        'has_igv' => (bool) $row->has_igv,
                        'calculate_quantity' => (bool) $row->calculate_quantity,
                        'item_unit_types' => collect($row->item_unit_types)->transform(function($row) {
                            return [
                                'id' => $row->id,
                                'description' => "{$row->description}",
                                'item_id' => $row->item_id,
                                'unit_type_id' => $row->unit_type_id,
                                'quantity_unit' => $row->quantity_unit,
                                'price1' => $row->price1,
                                'price2' => $row->price2,
                                'price3' => $row->price3,
                                'price_default' => $row->price_default,
                            ];
                        }),
                        'warehouses' => collect($row->warehouses)->transform(function($row) {
                            return [
                                'warehouse_id' => $row->warehouse->id,
                                'warehouse_description' => $row->warehouse->description,
                                'stock' => $row->stock,
                            ];
                        })
                    ];
                });
                return $items;

                break;
            default:
                return [];

                break;
        }
    }
    public function getFullDescription($row){

        $desc = ($row->internal_id)?$row->internal_id.' - '.$row->description : $row->description;
        $category = ($row->category) ? " - {$row->category->name}" : "";
        $brand = ($row->brand) ? " - {$row->brand->name}" : "";

        $desc = "{$desc} {$category} {$brand}";

        return $desc;
    }
    public function storecreate(ProcesoProducCreateRequest $request)
    {
        $id = $request->input('id');
        $proceso = ProcesoProduc::firstOrNew(['id' => $id]);
        $proceso->fill($request->all());
        $purchase_items =  PurchaseItem::all();
        $v_insumo = isset($proceso->insumo) ? $proceso->insumo:[];
        foreach ($v_insumo as $in) {
        $purchase_items =  PurchaseItem::where('purchase_id' , $in->compra)
        ->where('item->description' , $in->insum)->get();
        foreach ($purchase_items  as $purch) {
            $purchase_peso = $purch->item;
        foreach ($purchase_peso as $row) {
            $p_item = new PurchaseItem();
            //$p_item->fill($row);
            $p_item->purchase_id = $purch->purchase_id;
            $p_item->item_id = $purch->item_id;
            $p_item->save();

            if (array_key_exists('lots', $row)) {

                foreach ($row['lots'] as $lot) {

                    $p_item->lots()->create([
                        'date' => $lot['date'],
                        'series' => $lot['series']- $in->peso,
                        'peso'  => $lot['peso'],
                        'item_id' => null,
                        'warehouse_id' => $row['warehouse_id'],
                        'has_sale' => false
                    ]);
                }}
                else{
                    $peso = $purchase_items->quantity;
                    $peso = $peso - $in->peso;
                    $purch = PurchaseItem::where('purchase_id' , $in->compra)
                    ->where('item->description' , $in->insum)->update(['quantity' => $peso]);
                }
        }
            }
        }





            // foreach ($purchase_items  as $purch) {
            //         $purchase_peso = $purch->item->lots;
            //     if ($purchase_peso) {
            //         foreach ($purchase_peso as $peso) {    
            //             $peso_actualizado= $peso->peso - $in->peso;}
            //             $lots=array(
            //             'id' => $purch->item->lots[0]->id? $purch->item->lots[0]->id:'null',
            //             'date' => $purch->item->lots[0]->date,
            //             'peso' => $peso_actualizado,
            //             'index' => $purch->item->lots[0]->index,
            //             'state' => $purch->item->lots[0]->state,
            //             'series' => $purch->item->lots[0]->series,
            //             'item_id' => $purch->item->lots[0]->item_id? $purch->item->lots[0]->item_id:'null');
                        // $loteoes= 
                        //     '[{'."'id'".': ' . ($purch->item->lots[0]->id? $purch->item->lots[0]->id:'null').
                        //     ', '."'date'".': ' . $purch->item->lots[0]->date.
                        //     ', '."'peso'".': ' . $peso_actualizado.
                        //     ', '."'index'".': ' . $purch->item->lots[0]->index.
                        //     ', '."'state'".': ' . $purch->item->lots[0]->state.
                        //     ', '."'series'".': ' . $purch->item->lots[0]->series.
                        //     ', '."'item_id'".': ' . ($purch->item->lots[0]->item_id? $purch->item->lots[0]->item_id:'null').'}]';
                        // $lot=json_encode($lots,320);
                        // $lot=preg_replace('/\\\\\"/',"\"", $lot);
                        // $purchaseitem_peso = PurchaseItem::where('purchase_id' , $in->compra)
                        // ->where('item->description' , $in->insum)->update(['item->lots' =>json_encode($loteoes)]);
       
                       // $purchaseitem_peso = PurchaseItem::firstOrNew(['purchase_id' => $in->compra,'item->description' => $in->insum]);
        
                        //->where('item->description' , $in->insum);
       
                        // $purchaseitem_peso->fill($request->all());

        //$purchaseitem_peso->fill(['item->lots'=>"{$lot}"]);
        // foreach ($purchaseitem_peso->item->lots as $lote) {
        //     unset($lote->peso);
        //     $lote->peso=$peso_actualizado;
        //     echo json_encode($lote);
        //  }
        //     array_splice($purchaseitem_peso->item->lots,0,1);
        //     array_push($purchaseitem_peso->item->lots,$lots);
        //     echo is_array($purchaseitem_peso->item->lots) ? 'Array' : 'No es un array';
        //     echo json_encode($purchaseitem_peso->item->lots) , json_encode($lots);
        //     $purchaseitem_peso->save();
        //         }else{
        //             $peso = $purch->quantity;
        //             $peso = $peso - $in->peso;
        //             $purch = PurchaseItem::where('purchase_id' , $in->compra)
        //             ->where('item->description' , $in->insum)->update(['quantity' => $peso]);
        //         }
        //     }
        // }
        // $proceso->save();

        // $v_lots = isset($request) ? $request:[];

        //     foreach ($v_lots as $lot) {

        //         // $item->lots()->create($lot);
        //         $proceso->create([
        //             'op'=> $lot['op'],
        //             'producto_final' => $lot['producto_final'],
        //             'fecha_inicio'=> $lot['init'],
        //             'fecha_final'=> $lot['llegada'],
        //             'hilo'=> $lot['hilo'],
        //             'partida'=> $lot['partida'],
        //             'produc_artic'=> $lot['produc_artic'],
        //             'color'=> $lot['color'],
        //             'prov_tejed'=> $lot['prov_tejed'],
        //             'warehouses_id' => $lot['warehouses_id'],
        //             'cantidad'=> $lot['cantidad'],
        //             'peso'=> $lot['peso'],
        //             'peso_tej'=> $lot['peso_tej'],
        //             'peso_tin'=> $lot['peso_tin'],
        //             'tejed'=> $lot['tejed'],
        //             'tinto'=> $lot['tinto'],
        //             'estado' => $lot['estado'],
        //             'prov_tejed' => $lot['prov_tejed'],
        //             'prov_tin' => $lot['prov_tin'],
        //             'guia_tinto' => $lot['guia_tinto'],
        //             'guia_teje' => $lot['guia_teje'],
        //             'num_rollos' => $lot['num_rollos'],
                    
        //         ]);
        //     }
        //     $proceso->update();
        return [
            'success' => true,
            'message' => ($id)?'Proceso editado con éxito':'Proceso registrado con éxito'
        ];
    }
    public function storeimport(ProcesoProducImportRequest $request)
    {
        $id = $request->input('id');
        $proceso = ProcesoProduc::firstOrNew(['id' => $id]);
        $proceso->fill($request->all());
        $proceso->estado="Tejeduría";
        $proceso->update();

        return [
            'success' => true,
            'message' => ($id)?'Proceso editado con éxito':'Proceso registrado con éxito'
        ];
    }
    public function storereturnimport(Request $request)
    {
        $id = $request->input('id');
        $proceso = ProcesoProduc::firstOrNew(['id' => $id]);
        $proceso->fill($request->all());
        $proceso->estado="Importación";
        $proceso->update();

        return [
            'success' => true,
            'message' => ($id)?'Proceso editado con éxito':'Proceso registrado con éxito'
        ];
    }
    public function storetej(ProcesoProducTejRequest $request)
    {
        $id = $request->input('id');
        $proceso = ProcesoProduc::firstOrNew(['id' => $id]);
        $proceso->fill($request->all());
        $proceso->estado="Tintorería";
        $proceso->update();

        return [
            'success' => true,
            'message' => ($id)?'Proceso editado con éxito':'Proceso registrado con éxito'
        ];
    }
    public function storereturntej(Request $request)
    {
        $id = $request->input('id');
        $proceso = ProcesoProduc::firstOrNew(['id' => $id]);
        $proceso->fill($request->all());
        $proceso->estado="Tejeduría";
        $proceso->update();

        return [
            'success' => true,
            'message' => ($id)?'Proceso editado con éxito':'Proceso registrado con éxito'
        ];
    }
    public function storetin(ProcesoProducTinRequest $request)
    {
        $id = $request->input('id');
        $proceso = ProcesoProduc::firstOrNew(['id' => $id]);
        $proceso->fill($request->all());
        $proceso->estado="Inventario";
        $proceso->update();

        return [
            'success' => true,
            'message' => ($id)?'Proceso editado con éxito':'Proceso registrado con éxito'
        ];
    }
    public function storeingreso(ProcesoProducTinRequest $request)
    {
        // $id = $request->input('id');
        // $proceso = ProcesoProduc::firstOrNew(['id' => $id]);
        // $proceso->fill($request->all());
        // $proceso->update();

        // return [
        //     'success' => true,
        //     'message' => ($id)?'Proceso editado con éxito':'Proceso registrado con éxito'
        // ];
        $id = $request->input('id');
        $proceso = ProcesoProduc::firstOrNew(['id' => $id]);
        $proceso->fill($request->all());
       // $proceso->estado="Inventario";
        $proceso->update();
        $item = Item::firstOrNew(['description' => $proceso->produc_artic]);
        $item->stock = $item->stock + $request->num_rollos - $request->roll_rest;
        $item->warehouses()->update([
            'stock' => $item->stock ,
        ]);
        //$item->fill($request->all());
            // $item->lots()->delete();
            $establishment = Establishment::where('id', auth()->user()->establishment_id)->first();
            $warehouse = Warehouse::where('establishment_id',$establishment->id)->first();

            //$warehouse = WarehouseModule::find(auth()->user()->establishment_id);

            // $v_lots = isset($request->lots) ? $request->lots:[];
            $v_ingreso = isset($request->ingreso) ? $request->ingreso:[];
            foreach ($v_ingreso as $ingreso){
                $v_lots = isset($ingreso['lots']) ? $ingreso['lots']:[];
                foreach ($v_lots as $lot) {
                    $item->lots()->create([
                        'date' => $lot['date'],
                        'series' => $lot['series'],
                        'peso' => $lot['peso'],
                        'item_id' => $item->id,
                        'warehouse_id' => $warehouse ? $warehouse->id:null,
                        'has_sale' => false,
                        'state' => $lot['state'],
                    ]);
                }
            }
            // foreach ($v_lots as $lot) {

            //     // $item->lots()->create($lot);
            //     $item->lots()->create([
            //         'date' => $lot['date'],
            //         'series' => $lot['series'],
            //         'peso' => $lot['peso'],
            //         'item_id' => $item->id,
            //         'warehouse_id' => $warehouse ? $warehouse->id:null,
            //         'has_sale' => false,
            //         'state' => $lot['state'],
            //     ]);
            // }
        $item->update();
        return [
            'success' => true,
            'message' => ($id)?'Proceso editado con éxito':'Proceso registrado con éxito'
        ];
    }
    public function storereturntin(Request $request)
    {
        $id = $request->input('id');
        $proceso = ProcesoProduc::firstOrNew(['id' => $id]);
        $proceso->fill($request->all());
        $proceso->estado="Tintorería";
        $proceso->update();

        return [
            'success' => true,
            'message' => ($id)?'Proceso editado con éxito':'Proceso registrado con éxito'
        ];
    }
    public function storealm(ProcesoProducAlmRequest $request)
    {
        $id = $request->input('id');
        $proceso = ProcesoProduc::firstOrNew(['id' => $id]);
        $proceso->fill($request->all());
        $proceso->estado="Inventario";
        $proceso->update();
        $item = Item::firstOrNew(['description' => $proceso->produc_artic]);
        $item->stock = $item->stock + $request->num_rollos - $request->roll_rest;
        $item->warehouses()->update([
            'stock' => $item->stock ,
        ]);
        //$item->fill($request->all());
            // $item->lots()->delete();
            $establishment = Establishment::where('id', auth()->user()->establishment_id)->first();
            $warehouse = Warehouse::where('establishment_id',$establishment->id)->first();

            //$warehouse = WarehouseModule::find(auth()->user()->establishment_id);

            // $v_lots = isset($request->lots) ? $request->lots:[];
            $v_ingreso = isset($proceso->ingreso) ? $proceso->ingreso:[];
            foreach ($v_ingreso as $ingreso){
                $v_lots = isset($ingreso['lots']) ? $ingreso['lots']:[];
                foreach ($v_lots as $lot) {
                    $item->lots()->create([
                        'date' => $lot['date'],
                        'series' => $lot['series'],
                        'peso' => $lot['peso'],
                        'item_id' => $item->id,
                        'warehouse_id' => $warehouse ? $warehouse->id:null,
                        'has_sale' => false,
                        'state' => $lot['state'],
                    ]);
                }
            }
            // foreach ($v_lots as $lot) {

            //     // $item->lots()->create($lot);
            //     $item->lots()->create([
            //         'date' => $lot['date'],
            //         'series' => $lot['series'],
            //         'peso' => $lot['peso'],
            //         'item_id' => $item->id,
            //         'warehouse_id' => $warehouse ? $warehouse->id:null,
            //         'has_sale' => false,
            //         'state' => $lot['state'],
            //     ]);
            // }
        $item->update();
        return [
            'success' => true,
            'message' => ($id)?'Proceso editado con éxito':'Proceso registrado con éxito'
        ];
    }
    public function storedet(Request $request)
    {
        $id = $request->input('id');
        $proceso = ProcesoProduc::firstOrNew(['id' => $id]);
        $proceso->fill($request->all());
        $proceso->estado="Cancelado";
        $proceso->update();

        return [
            'success' => true,
            'message' => ($id)?'Proceso editado con éxito':'Proceso registrado con éxito'
        ];
    }
    public function storeSeries(ItemRequest $request) {

        $id = $request->input('id');
        $item = Item::firstOrNew(['id' => $id]);
        $item->fill($request->all());
        if ($id) {

            // $item->lots()->delete();
            $establishment = Establishment::where('id', auth()->user()->establishment_id)->first();
            $warehouse = Warehouse::where('establishment_id',$establishment->id)->first();

            //$warehouse = WarehouseModule::find(auth()->user()->establishment_id);

            $v_lots = isset($request->lots) ? $request->lots:[];
            
            foreach ($v_lots as $lot) {

                // $item->lots()->create($lot);
                $item->lots()->create([
                    'date' => $lot['date'],
                    'series' => $lot['series'],
                    'item_id' => $item->id,
                    'warehouse_id' => $warehouse ? $warehouse->id:null,
                    'has_sale' => false,
                    'state' => $lot['state'],
                ]);
            }
        } 

        $item->update();
        
        return [
            'success' => true,
            'message' => ($id)?'Producto editado con éxito':'Producto registrado con éxito',
            'id' => $item->id
        ];
    }

    

}
